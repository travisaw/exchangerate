<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Exchange Rates</title>
	<link rel="stylesheet" type="text/css" href="default.css">
</head>
<body>

<h1>Exchange Rate Summary</h1>

<?php

echo "<h3>Times reported from zone: " . date('e') . " - " . date('T') . "</h3>\n";

require_once 'table.php';

$curDetail = 0;

if (empty($_GET['cur'])) {
	$sqlSummary = "SELECT * FROM ExchWebDetails;";
	}
else {
	$curDetail = 1;
	echo "<h4>History for: " . $_GET['cur'] . "</h4>\n";
	$sqlSummary =
		"SELECT fromCurName AS `From`, " .
				"toCurName `To`, " .
				"rate AS `Rate`, " .
				"price AS `Price`, " .
				"CASE direction WHEN 1 THEN 'UP' WHEN -1 THEN 'DOWN' ELSE 'SAME' END AS `Direction`, " .
				"DATE_FORMAT(`published`, '%W %M %D %Y %r') AS `Time Published` " .
		"FROM ExchDetails " .
		"WHERE fromCur = '" . $_GET['cur'] . "' " .
		"ORDER BY published DESC " .
		"LIMIT 200;";
	}

getTable($sqlSummary);

/* If this is the currency detail screen then provide like to get back to summary */
if ($curDetail == 1) {echo "\n<br><a href='report.php'>Back to Summary</a>";}

?>
</body>
</html>