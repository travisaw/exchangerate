<?php

//This holds connection string and connectes to DB

/* OO MYSQLi class */

$DBSERVER = "<SERVER>";
$DBNAME = "<DATABASE>";
$DBUSER = "<USERNAME>";
$DBPASS = "<PASSWORD>";

$con = new mysqli($DBSERVER, $DBUSER, $DBPASS, $DBNAME);
if ($con->connect_errno) {
	die ("Failed to connect to MySQL: (" . $con->connect_errno . ") " . $con->connect_error);
}

?>
