
/* ---------------------------------------------------------------------------------------------------------- */
-- CREATE ExchCur
CREATE TABLE IF NOT EXISTS `ExchCur` (
  `cur` varchar(5) NOT NULL,
  `curName` varchar(50) DEFAULT NULL,
  `curLocation` varchar(50) DEFAULT NULL,
  `symbol` char(4) DEFAULT NULL,
  `source` tinyint(4) NOT NULL DEFAULT '0',
  `sort` tinyint unsigned NOT NULL DEFAULT '255',
  PRIMARY KEY (`cur`)
) ENGINE=MyISAM DEFAULT
  COLLATE='utf8_general_ci'
  COMMENT='Stores Exchange Currency';

/* ---------------------------------------------------------------------------------------------------------- */
-- POPULATE ExchCur
INSERT INTO `ExchCur` (`cur`, `curName`, `curLocation`, `symbol`, `source`, `sort`) VALUES
	('USD', 'U.S. Dollar', 'United States', '$', 1, 255),
	('EUR', 'Euro', 'Europe', '€', 0, 255),
	('GBP', 'Pound Sterling', 'United Kingdom', '£', 0, 255),
	('ZAR', 'South African rand', 'South Africa', 'R', 0, 255),
	('JPY', 'Japanese yen', 'Japan', '¥', 0, 255),
	('CNY', 'Chinese yuan', 'China', '¥', 0, 255),
	('THB', 'Thai baht', 'Thailand', '฿', 0, 255),
	('CHF', 'Swiss franc', 'Switzerland', 'Fr', 0, 255),
	('SEK', 'Swedish krona', 'Sweden', 'kr', 0, 255),
	('NOK', 'Norwegian krone', 'Norway', 'kr', 0, 255),
	('NZD', 'New Zealand dollar', 'New Zealand', '$', 0, 255),
	('MXN', 'Mexican peso', 'Mexico', '$', 0, 255),
	('KRW', 'South Korean won', 'South Korea', '₩', 0, 255),
	('ILS', 'Israeli new shekel', 'Israel', '₪', 0, 255),
	('INR', 'Indian rupee', 'India', '$', 0, 255),
	('HKD', 'Hong Kong dollar', 'Hong Kong', '$', 0, 255),
	('BRL', 'Brazilian real', 'Brazil', 'R$', 0, 255),
	('AUD', 'Australian dollar', 'Australia', '$', 0, 255),
	('VEF', 'Venezuelan bolívar', 'Venezuela', 'Bs F', 0, 255),
	('AED', 'United Arab Emirates dirham', 'United Arab Emirates', 'د.إ', 0, 255),
	('TRY', 'Turkish lira', 'Turkey', 'T', 0, 255);

/* ---------------------------------------------------------------------------------------------------------- */
-- CREATE ExchRate
CREATE TABLE IF NOT EXISTS `ExchRate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rateFrom` varchar(5) NOT NULL,
  `rateTo` varchar(5) NOT NULL,
  `rate` float unsigned NOT NULL,
  `rateValue` float unsigned NOT NULL,
  `rateCurrent` tinyint(4) NOT NULL,
  `rateDirection` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1
  COLLATE='utf8_general_ci'
  COMMENT='Stores Exchange Rates';


/* ---------------------------------------------------------------------------------------------------------- */
DELIMITER $$$
/* Procedure updateRecipeIngredient - used to get ingredient id - if id not found create ingredient */
DROP PROCEDURE IF EXISTS `insertExchRate` $$$
CREATE PROCEDURE `insertExchRate`
   (IN `inFromRate` VARCHAR(5),
    IN `inToRate` VARCHAR(5),
	IN `inRate` FLOAT,
	IN `inValue` FLOAT
	)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Insert Exchange Rate'
BEGIN

	DECLARE varOldRate FLOAT;
	DECLARE varDirection SMALLINT;

	SELECT rate INTO varOldRate FROM ExchRate WHERE `rateFrom` = inFromRate AND `rateTo` = inToRate AND `rateCurrent` = 1;
	
	IF varOldRate > inRate THEN SET varDirection = -1;
	ELSEIF varOldRate < inRate THEN SET varDirection = 1;
	ELSE SET varDirection = 0;
	END IF;	

	UPDATE ExchRate
	SET `rateCurrent` = 0
	WHERE `rateFrom` = inFromRate
		AND `rateTo` = inToRate;

	INSERT INTO ExchRate (`rateFrom`, `rateTo`, `rate`, `rateValue`, `rateCurrent`, `rateDirection`)
	VALUES (inFromRate, inToRate, inRate, inValue, 1, varDirection);
	
END
$$$

/* ---------------------------------------------------------------------------------------------------------- */
/* View to summarize exchangce rate details */
CREATE OR REPLACE VIEW `ExchDetails`
AS
SELECT B.`curName` AS `fromCurName`,
		 B.`cur`		 AS `fromCur`,
		 C.`curName` AS `toCurName`,
		 C.`cur`		 AS `toCur`,
		 A.`rate`	 AS `rate`,
		 A.`published` AS `published`,
		 A.`rateCurrent` AS `current`,
		 A.`rateDirection` AS `direction`,
		 1 / rate as `price`
FROM ExchRate A
INNER JOIN ExchCur B
	ON A.`rateTo` = B.`cur`
INNER JOIN ExchCur C
	ON A.`rateFrom` = C.`cur`
ORDER BY B.`sort`;

/* ---------------------------------------------------------------------------------------------------------- */
/* View to drive the summary main page - report.php */
/* Dependent on View above */
CREATE OR REPLACE VIEW `ExchWebDetails`
AS
SELECT concat('<a href=''report.php?cur=', `fromCur`, '''>', `fromCurName`, ' (', `fromCur`, ')','<a>') AS `From Currency`,
		 concat(toCurName, ' (', toCur, ')') AS `To Currency`,
		 `rate` AS Rate,
		 `price` AS `Price per unit`,
		 DATE_FORMAT(`published`, '%W %M %D %Y %r') AS `Published`
FROM ExchDetails
WHERE current = 1;
