<?php

/*Accepts SQL query, returns result as an HTML table*/
function getTable($query)
{

	require 'dbconn.php';

	global $getTable_NullResult;
	$getTable_NullResult = 0;
	/*Table head*/
	echo "<table>";
	echo "\n\t<tr>";

	/*Execute Query*/
	$result = $con->query($query);
	if (!$result) {
		die('Query failed from source: ' . $con->error);
	}

	/* get column metadata */
	$cCount = 0;

	/* Populate header row and count number of columns*/

	$meta = $result->fetch_fields();

	foreach($meta as $field) {
		echo "\n\t\t<th>&nbsp;$field->name&nbsp;</th>";
	}

	$cCount = $result->field_count;

	echo "\n\t</tr>";
	/*figure out how to put row between header and data*/
	/*Fill rows*/
	$rowcount = 1;
	while($row = $result->fetch_row())
	{
		$tCount = 0;
		if (is_null($row[$tCount]) && $rowCount = 1) $getTable_NullResult = 1;
		echo "\n\t<tr>";
		while($tCount < $cCount) {
			echo "\n\t\t<td>&nbsp;$row[$tCount]&nbsp;</td>";
			$tCount++;
		}
		echo "\n\t</tr>";
		$rowcount++;
	}

	$result->close();

	/*Table foot*/
	echo "\n</table>\n";
}

?>
