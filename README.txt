
 *********************************************************************************************************
 *
 *  EXCHANGE RATE - RETREIVE AND REPORT
 *
 *  BY: TRAVIS WILSON (travis@totaltravisty.com)
 *  v1.0 08/21/2013
 *
 *  CONTENTS:
 *    1) INTRODUCTION
 *    2) EXCHANGE RATE SOURCE
 *    3) INCLUDED CURRENCIES
 *    4) FILE DESCRIPTION
 *    5) CONFIG DETAILS
 *
 *********************************************************************************************************
 
 
 *********************************************************************************************************
 1) INTRODUCTION
 *********************************************************************************************************
 There are two pieces to this package.  One is a PHP script (exrate.php) that will retrieve the exchange 
 rates to update (From Table: ExchRate) and save the rates with a timestamp (To Table: ExchDetails).
 In the ExchRate table there is a list of the offical currency abbriviations to be converted.
 Only one currency should be set at the source.  This is the currency that all other currencies will be 
 converted ‘to’.

 
 *********************************************************************************************************
 2) EXCHANGE RATE SOURCE
 *********************************************************************************************************
 Data comes from Google Finance via API
 API details:
 rate-exchange.appspot.com
 Query URL:
 http://rate-exchange.appspot.com/currency?from=EUR&to=USD&q=1
 API Source:
 https://github.com/hippasus/ExchangeRates
 
 *********************************************************************************************************
 3) INCLUDED CURRENCIES
 *********************************************************************************************************
 AED|United Arab Emirates dirham
 AUD|Australian dollar
 BRL|Brazilian real
 CHF|Swiss franc
 CNY|Chinese yuan
 EUR|Euro
 GBP|Pound Sterling
 HKD|Hong Kong dollar
 ILS|Israeli new shekel
 INR|Indian rupee
 JPY|Japanese yen
 KRW|South Korean won
 MXN|Mexican peso
 NOK|Norwegian krone
 NZD|New Zealand dollar
 SEK|Swedish krona
 THB|Thai baht
 TRY|Turkish lira
 USD|U.S. Dollar
 VEF|Venezuelan bolívar
 ZAR|South African rand

 *********************************************************************************************************
 4) FILE DESCRIPTION
 *********************************************************************************************************
 
 datainstall.sql
 	SQL script for MySQL to create data schema a views.

 dbconn.php
 	File containing database connection used by exrate.php and report.php.

 default.css
 	Stylesheet used by report.php

 exrate.php
 	Script to retreive exchange rates.  This script should be run from the command line or by a chron job.

 README.txt
 	This File

 report.php
 	Web page that displays exchange rate details.  Put this in your website.

 table.php
 	PHP utility to draw tables used in reporting.  Requires dbconn.php.

 *********************************************************************************************************
 5) CONFIG DETAILS
 *********************************************************************************************************
    1) Run datainstall.sql on the database which will store the data.
 
    2) Change connection string details in dbconn.php
 
    3) dbconn.php, default.css, report.php and table.php should be put in a folder accessable by your websever.
 
    4) exrate.php and a copy of dbconn.php can be put wherever you like - in a folder with other cron jobs.
       If you prefer you can put the connection string details in the top of exrate.php and skip the
       dbconn.php copy all together.

