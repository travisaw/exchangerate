<?php

/*********************************************************************************************************/
/* CONNECTION DETAILS */

/*
$DBSERVER = "<SERVER>";
$DBNAME = "<DATABASE>";
$DBUSER = "<USERNAME>";
$DBPASS = "<PASSWORD>"

$con = new mysqli($DBSERVER, $DBUSER, $DBPASS, $DBNAME);
if ($con->connect_errno) {
	die ("Failed to connect to MySQL: (" . $con->connect_errno . ") " . $con->connect_error);
}
*/

require_once 'dbconn.php';

/*********************************************************************************************************/
/* Get Source Currency*/

$sqlFrom = "SELECT cur FROM ExchCur WHERE source = 1 LIMIT 1;";

if (!$result = $con->query($sqlFrom)) {
	die ("CALL failed: (" . $con->errno . ") " . $con->error);
	}
if ($result->num_rows > 0) {
	$sourceRow = $result->fetch_row();
	$curSource = $sourceRow[0];
	}

/*********************************************************************************************************/
/* Get Array of Destination Currencies */

$sqlTo = "SELECT cur FROM ExchCur WHERE source = 0;";

if (!$result = $con->query($sqlTo)) {
	die ("CALL failed: (" . $con->errno . ") " . $con->error);
	}
if ($result->num_rows > 0) {
	$curCount = 0;
	while ($row = $result->fetch_row()) {
		$curDest[$curCount] = $row[0];
		$curCount++;
		}
	}

/*********************************************************************************************************/
/* Get Rates*/

$loopCount = 0;

while ($loopCount < $curCount) {

	$url="http://rate-exchange.appspot.com/currency?from=" . $curSource . "&to=" . $curDest[$loopCount] ."&q=1";
	$data = NULL;
	$data = @file_get_contents($url);
	if($data)
	{

		$obj = json_decode($data);

		/* IF RESULTS ARE EMPTY THEN DON'T ATTEMPT TO SAVE */
		if (!empty($obj->{'to'})) {

			/* DEBUG - DISPLAY RESULTS OF EACH CURRENCY CONVERSION
			echo "<br><br>";
			echo "TO: " . $obj->{'to'};
			echo "<br>";
			echo "FROM: " . $obj->{'from'};
			echo "<br>";
			echo "RATE: " . $obj->{'rate'};
			echo "<br>";
			echo "VALUE: " . $obj->{'v'};  */

			$curInsert = "CALL `insertExchRate`('" . $obj->{'from'} . "', '" . $obj->{'to'} . "', " . $obj->{'rate'} . ", " . $obj->{'v'} . ");";

			if (!$con->query($curInsert)) {
				die ("CALL failed: (" . $con->errno . ") " . $con->error); }
			/*else {
				echo "Submitted Record";
			}*/
		}
	}
	$loopCount++;
	sleep(1);
}

?> 